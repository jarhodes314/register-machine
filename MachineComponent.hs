module MachineComponent where

import RegisterMachine
import Data.Array
import Data.Array.IO
import Control.Monad
import Control.Monad.State
import Debug.Trace

type HighLevelInstruction = State Line [Instruction]

-- Takes a source, destination and scratch space
-- Returns a list of instructions and the following line number
copy :: RegAddr -> RegAddr -> RegAddr -> HighLevelInstruction
copy addr addr' scratch = do
    start <- get
    put (start + 7)
    return [
        DEC addr'   start       (start + 1), -- zero the destination register
        DEC scratch (start + 1) (start + 2), -- zero the scratch space
        DEC addr    (start + 3) (start + 5),
        INC addr'   (start + 4),
        INC scratch (start + 2),             -- keep a copy to move back to addr
        DEC scratch (start + 6) (start + 7), -- copy scratch
        INC addr    (start + 5)]             -- ... into addr

-- Takes two sources, a destination and scratch space
-- Result is addr `div` addr'
regdiv :: RegAddr -> RegAddr -> RegAddr -> RegAddr -> HighLevelInstruction
regdiv addr addr' addrR scratch = do
    start <- get
    put (start + 8)
    return [
        DEC scratch start       (start + 1), -- zero the scratch space
        DEC addrR   (start + 1) (start + 2), -- zero the result
        DEC addr'   (start + 3) (start + 5),
        DEC addr    (start + 4) (start + 8),
        INC scratch (start + 2),             -- keep a copy of y here
        INC addrR   (start + 6),             -- add one to the result since we have subtracted y
        DEC scratch (start + 7) (start + 2), -- copy y back
        INC addr'   (start + 6)]             -- ^^^

-- Takes two sources, a destination and scratch space
-- Result is addr `mod` addr'
regmod :: RegAddr -> RegAddr -> RegAddr -> RegAddr -> HighLevelInstruction
regmod addr addr' addrR scratch = regdiv addr addr' scratch addrR 

-- Like copy, but doesn't zero the destination first
add :: RegAddr -> RegAddr -> RegAddr -> HighLevelInstruction
add addr addr' scratch = do
    start <- get
    put (start + 6)
    return [
        DEC scratch start       (start + 1),
        DEC addr    (start + 2) (start + 4),
        INC addr'   (start + 3),
        INC scratch (start + 1),
        DEC scratch (start + 5) (start + 6),
        INC addr    (start + 4)]

-- Performs x = x - y, or x = 0 if y > x
sub :: RegAddr -> RegAddr -> HighLevelInstruction
sub addr addr' = do
    start <- get
    put (start + 2)
    return [
        DEC addr' (start + 1) (start + 2),
        DEC addr  start       (start + 2)]

-- Multiply, taking two sources, a destination and a scratch register
-- Destroys the first argument
mul :: RegAddr -> RegAddr -> RegAddr -> RegAddr -> HighLevelInstruction
mul addr addr' addrR scratch = do
    start <- get
    put (start + 1)
    insAdd <- add addr' addrR scratch 
    l' <- get
    put (l' + 1)
    return ((DEC  addr (start + 1) (l' + 1)) : insAdd ++ [DEC  addr (start + 1) (l' + 1)])

-- Takes an address and an immediate value and sets the register to that value
set :: RegAddr -> Int -> HighLevelInstruction
set addr n = do
    z <- zero addr
    incs <- replicateM n inc
    return $ concat (z : incs)

  where

    inc :: HighLevelInstruction
    inc = do
        line <- get
        put (line + 1)
        return [INC addr (line + 1)]

    zero :: RegAddr -> HighLevelInstruction
    zero addr = do
        l <- get
        put (l+1)
        return [DEC addr l (l + 1)]

move :: RegAddr -> RegAddr -> HighLevelInstruction
move addr addr' = do
    start <- get
    put (start + 3)
    return [
        DEC addr' start       (start + 1),
        DEC addr  (start + 2) (start + 3),
        INC addr' (start + 1)]

-- TODO test these
sizeof :: HighLevelInstruction -> Line
sizeof ins = (execState ins 0) - 1

sizeofs :: [HighLevelInstruction] -> Line
sizeofs inss = (execState (compose inss) 0) - 1

log2 :: RegAddr -> RegAddr -> RegAddr -> RegAddr -> RegAddr -> HighLevelInstruction
log2 addr addr' scratch scratch' scratch'' = do
    start <- get
    compose [
        set scratch 2,
        regdiv addr scratch addr' scratch',
        inc scratch'',
        move addr' addr,
        abranchnz addr start, -- branch by one if quotient not zero
        move scratch'' addr',
        dec  addr']
  where
    branchnz :: RegAddr -> Line -> HighLevelInstruction
    branchnz addr offset = do
        start <- get
        put (start + 2)
        if offset < 0
        then return [
            DEC addr (start + 1) (start + 2),
            INC addr (start + 2 + offset)]
        else return [
            DEC addr (start + 1) (start + 2),
            INC addr (start + offset)]

    abranchnz :: RegAddr -> Line -> HighLevelInstruction
    abranchnz addr line = do
        start <- get
        put (start + 2)
        return [
            DEC addr (start + 1) (start + 2),
            INC addr line]

    inc :: RegAddr -> HighLevelInstruction
    inc addr = do
        start <- get
        put (start + 1)
        return [INC addr (start + 1)]

    dec :: RegAddr -> HighLevelInstruction
    dec addr = do
        start <- get
        put (start + 1)
        return [DEC addr (start + 1) (start + 1)]

compose :: [HighLevelInstruction] -> HighLevelInstruction
compose xs = sequence xs >>= return . join

compile :: [HighLevelInstruction] -> [Instruction]
compile xs = join (evalState (sequence xs) 0)

test :: IO ()
test = do
    let ins = compile [set 2 2047, log2 2 0 4 5 6]
    --mapM_ print ins
    regs <- runInstructions (listArray (0, fromIntegral (length ins - 1)) ins)
    inspectRegisters regs
