module RegisterMachine (
    RegAddr,
    Instruction (HALT, INC, DEC),
    Line,
    runInstructions,
    inspectRegisters
    ) where

import Data.Array
import Data.Array.IO
import Control.Monad
import Control.Monad.State

type Line = Integer
type RegAddr = Integer
type RegVal = Integer
data Instruction = HALT | INC RegAddr Line | DEC RegAddr Line Line deriving Show
type Registers = IOArray RegAddr RegVal 

-- Create n registers, all initialised to 0
createRegisters :: Integer -> IO Registers
createRegisters n = newArray (0,n-1) 0

-- Iterate over an array of register values, copying them to a larger array
copyRegisters :: Registers -> Registers -> IO () 
copyRegisters regs regs' = do
    (lb,ub) <- getBounds regs
    forM_ [lb..ub] (\i -> readArray  regs  i >>= 
                          writeArray regs' i)

-- Check whether the address being accessed is in the bounds of the register array
-- If it isn't, create a new set of registers and copy the old ones over
createRegistersIfNeeded :: RegAddr -> StateT Registers IO ()
createRegistersIfNeeded addr = do
    regs <- get
    bounds <- lift $ getBounds regs
    if snd bounds < addr
    then do
        regs' <- lift $ createRegisters (addr+1)
        lift $ copyRegisters regs regs'
        put regs'
    else return ()

runInstruction :: Instruction -> StateT Registers IO [Line]
runInstruction HALT = return []
runInstruction (INC addr target) = do
    createRegistersIfNeeded addr
    regs <- get
    currentVal <- lift $ readArray regs addr
    lift $ writeArray regs addr (currentVal + 1)
    -- no need to run put, we are using an IOArray
    return [target]
runInstruction (DEC addr succTarget failTarget) = do
    createRegistersIfNeeded addr
    regs <- get
    currentVal <- lift $ readArray regs addr
    case currentVal of
        0 -> return [failTarget]
        n -> do
            lift $ writeArray regs addr (n-1)
            return [succTarget]

runInstructions :: Array Line Instruction -> IO Registers
runInstructions ins = createRegisters 1 >>= execStateT (getAndRun 0) where
    getAndRun :: Line -> StateT Registers IO ()
    getAndRun n
     | n > snd (bounds ins) = return ()
     | otherwise = do
        ls <- runInstruction (ins!n)
        case ls of
            [] -> return ()
            _  -> getAndRun (head ls)

inspectRegisters :: Registers -> IO()
inspectRegisters regs = do
    (lb, ub) <- getBounds regs
    forM_ [lb..ub] (\i -> putStr ('R' : show i ++ " : ") >> readArray regs i >>= print)

test :: IO ()
test = do
    regs <- runInstructions (listArray (0,2) [INC 0 1, DEC 1 3 2, INC 2 3])
    inspectRegisters regs

